path = require "path"
app = require path.join path.dirname(require.main.filename), "./riot"
  .app
riot = require path.join path.dirname(require.main.filename), "./riotjs"
global.extend = extend = `function (){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||m.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(e=arguments[h]))for(d in e)a=g[d],c=e[d],g!==c&&(j&&c&&(m.isPlainObject(c)||(b=m.isArray(c)))?(b?(b=!1,f=a&&m.isArray(a)?a:[]):f=a&&m.isPlainObject(a)?a:{},g[d]=m.extend(j,f,c)):void 0!==c&&(g[d]=c));return g}`
config = require path.join path.dirname(require.main.filename), "./config"
mongoose = require "mongoose"
mongoose.connect config.db
for module in config.modules
  if module.path?
    try
      app require "./modules/" + module.path
      console.log module.name + " module included"
    catch error
      throw error

app
  env: process.env
  config: config
  riot: riot