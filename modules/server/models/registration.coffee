mongoose = require "mongoose"
Schema = mongoose.Schema

Registration = new Schema
  email: String
  instagram_account: String
  name: String
  size: String
  tsize: String
  place: String
  type: String
  created:
    type: Date
    default: Date.now


module.exports = mongoose.model "Registration", Registration