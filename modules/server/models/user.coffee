mongoose = require "mongoose"
Schema = mongoose.Schema

User = new Schema
  runningId: String

  instagramId: String
  instagramName: String

  hash: String

  secure:
    login: String
    password: String


  train:
    shoes:
      type: String
      default: null
    shirt:
      type: String
      default: null
    place:
      type: String
      default: null

module.exports = mongoose.model "User", User