path = require "path"
express = require "express"
http = require "http"
app = express()
socket = require "socket.io"
bodyParser = require "body-parser"
session = require "express-session"
cookie = require "cookie-parser"
#cookieSession = require "cookie-session"
config = require "./config"
appConfig = require path.join path.dirname(require.main.filename), "./config"
MongoStore = require("connect-mongo")(session)
globData = {}
crypto = require "crypto"
User = require "./models/user"
device = require "express-device"
Registration = require "./models/registration"
Stolen = require "./models/stolen"
emailjs = require "emailjs"
jade = require "jade"


module.exports = (data) ->
  self = data.riot.observable(@)
  server = null
  app.set 'port', data.env.PORT || 4013
  .set 'views', path.join path.dirname(require.main.filename), './views'
    .set 'view engine', 'jade'
      .use express.static path.join path.dirname(require.main.filename), './public'
  app.use bodyParser.urlencoded()
  app.use bodyParser.json()
  app.use require("morgan")('dev') if data.env.ENV is "dev"
  app.use session
    secret: config.secret
    cookie:
      domain: appConfig.domain
    store: new MongoStore
      db: "charge"

  app.use device.capture()
  device.enableViewRouting app,
    "noPartials": true

  app.use cookie config.secret
  ,
    domain: appConfig.domain

  #  app.use cookieSession
  #    key: 'app.sess'
  #    secret: config.secret,
  #    secure: false


  sendConfirmation = (reg, inspData) ->
    server = emailjs.server.connect
      user: "noreply@adidas-running.ru"
      password: "88LGFxUPE"
      host: "smtp.yandex.ru"
      ssl: true
      port: 465

    vars =
      mediaRoot: "http://energy.adidas-running.ru/img/"
      username: reg.instagram_account
      runclub: reg.place

    mailPath = if inspData.inspirator then "./views/server/confirmation-inspirator.jade" else "./views/server/confirmation.jade"
    text = jade.renderFile path.join(path.dirname(require.main.filename), mailPath), vars

    server.send
      text: text,
      from: "adidas-running <noreply@adidas-running.ru>"
      to: reg.email
      subject: "Шанс оказаться на забеге в Лиссабоне!"
      attachment: [
        data: text
        alternative: true
      ]
    ,
    (err, mess) ->
      if err
        console.log err, "server.send", text, reg
      console.log "Server sended fucking message"

  self.one "load", ->
    for method, routes of config.routes
      for pathname, route of routes
        self.addRoute method, pathname, require "./routes/" + route

    console.log "Trying to make server on port " + app.get "port" if process.env.ENV is "dev"

    server = http.createServer app
    .listen app.get 'port'

    io = socket.listen server
    io.sockets.on "connection", (socket) ->

    self.addRoute "get", "/api/getIgData", (req, res) ->
      User.findOne {hash: req.cookies.runningId}, (err, user) ->
        unless user.instagramName
          noIg = true
          hash = crypto.createHash('md5').update(user.runningId + "_" + config.runningSecret).digest('hex')
          console.log "to hash", user.runningId + "_" + config.runningSecret, hash, config.api + "getInstagramData?sig=" + hash + "&user_id=" + user.runningId

          http.get config.api + "getInstagramData?sig=" + hash + "&user_id=" + user.runningId, (resp) ->
            text = ''
            resp.on "data", (chunk) ->
              text += chunk

            resp.on "end", ->
              igData = JSON.parse text
              console.log igData
              if igData.sig == "false"
                console.log "Sig err"
              else if igData.auth is "false"
                console.log "Auth err"
              else if igData.instagram_id != "0"

                noIg = false
                user.instagramName = igData.instagram_username
                user.instagramId = igData.instagram_id
                user.save()

              if noIg
                return res.json {ig: false}

              data.ig.checkUser user.instagramName, (result) ->
                res.json extend result, {ig: true, igUser: user.instagramName}
        else
          data.ig.checkUser user.instagramName, (result) ->
            res.json extend result, {ig: true, igUser: user.instagramName}

    indexRoute = (req, res) ->
      if req.params.user?
        data.ig.checkUser req.params.user, (result) ->
          if result.inspirator
            image = req.params.user + "-inspirator.jpg"
          else
            image = req.params.user + "-inspirator.jpg"
          res.render "server/graph"
          ,
            bounds: JSON.stringify data.bounder.bounds
            styles: ["/css/graph.css"]
            scripts: appConfig.rendering.scripts.concat ["/js/rainbowvis.js", "/js/gr4.js"]
            og_image: image


      else
        image = "share_1.jpg"
        res.render "server/graph"
        ,
          bounds: JSON.stringify data.bounder.bounds
          styles: ["/css/graph.css"]
          scripts: appConfig.rendering.scripts.concat ["/js/rainbowvis.js", "/js/gr4.js"]
          og_image: image

    self.addRoute "get", "/", indexRoute
    self.addRoute "get", "/media", indexRoute
    self.addRoute "get", "/user/:user", indexRoute

    self.addRoute "get", "/nf", (req, res) ->
      res.render "server/not_found"
    self.addRoute "get", "/registered", (req, res) ->
      res.render "server/registered", {inst: req.query.i}
    self.addRoute "post", "/registered", (req, res) ->
      st = new Stolen req.body
      st.save ->
        console.log "Saved stolen"
        res.redirect "/stolen"

    self.addRoute "get", "/inspirated", (req, res) ->
      res.render "server/inspirated"

    self.addRoute "get", "/stolen", (req, res) ->
      res.render "server/stolen"
    self.addRoute "get", "/inspirator", (req, res) ->
      res.render "server/inspirator"

    self.addRoute "post", "/reg", (req, res) ->
      Registration.find {instagram_account: req.body.instagram_account}, (err, regs) ->
        console.log err if err
        data.ig.checkUser req.body.instagram_account, (result) ->
          # Если юзер непричем - идет нахуй
          unless result.inspirated || result.inspirator
            res.redirect "/nf"
            return;
          # Если юзер уже регался
          if regs.length > 0

            res.redirect "/registered"
            return;

          reg = new Registration req.body
          reg.save()
          console.log reg
          res.redirect "/#" + reg.instagram_account
          sendConfirmation reg, result


    self.addRoute "get", "/igData", (req, res) ->
      data.ig.checkUser req.query.user, (result) ->
        Registration.find {instagram_account: req.query.user}, (err, accs) ->
          result.placed =
            inspirator: false
            inspirated: false
            fill: false
          # Если юзер уже регался
          if accs.length > 0
            # Чекаем вводил ли размеры
            for r in accs
              result.placed.inspirator = true if r.type is "inspirator"
              result.placed.inspirated = true if r.type is "inspirated"
              result.placed.fill = true if r.place?
          res.json result

    self.addRoute "get", "/used-email", (req, res) ->
      Registration.find {email: req.query.email}, (err, emails) ->
        console.log emails.length
        res.json emails.length is 0

    self.addRoute "get", "/howitworks", (req, res) ->
      res.render "server/howto"


    data.server = self
    data.io = io

  self.addRoute = (method, pathname, handler) ->
    switch method
      when "get" then app.get pathname, handler
      when "post" then app.post pathname, handler
      when "put" then app.put pathname, handler
      when "delete" then app.delete pathname, handler
      else
        throw "Unknown route method: " + method
    console.log method + " " + pathname

  self.updateUser = (hash, userData) ->
    console.log hash
    User.findOne {hash: hash}, (err, user) ->
      console.log err if err
      user.instagramId = userData.user.id
      user.instagramName = userData.user.username
      hash = crypto.createHash('md5').update(user.runningId + "_" + user.instagramId + "_" + config.runningSecret).digest('hex')
      http.get config.api + "setInstagramData?sig=" + hash + "&instagram_id=" + user.instagramId + "&instagram_username=" + user.instagramName + "&user_id=" + user.runningId, (resp) ->
        text = ''
        resp.on "data", (chunk) ->
          text += chunk

        resp.on "end", ->
          igData = JSON.parse text
          if igData.saved is "true"
            user.save()
          else
            console.log "Ext save error"


  self.trigger "load"

  globData = data