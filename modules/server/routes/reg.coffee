http = require "http"
crypto = require "crypto"
qs = require "querystring"
config = require "../config"
test = process.env.ENV is "dev"

module.exports = (req, res) ->
  vals = req.body
  console.log vals
  result = {}

  required = ["agree", "bdate", "city", "password", "confirm", "email", "name"]
  required.push "phone" if vals.cartrunclub?
  errors = []
  errFields = {fields: [], type: "fields"}
  for field in required
    errFields.fields.push field if not vals[field]? or vals[field] is ""

  errors.push errFields if errFields.fields.length > 0

  errors.push {type: "pass"} unless vals.password is vals['confirm']

  vals.sex = vals.sex.toString() if vals.sex?
  if errors.length > 0
    result.type = "errors"
    result.errors = errors
    res.json result
    console.log errors
  else
    result.status = "ok"
    console.log "no errors. get api req"
    vals.sig = crypto.createHash('md5').update(vals.email + "_" + vals.password + "_" + config.runningSecret).digest('hex')
    vals.test = true if test

    url = "http://adidas-running.ru/api/reg?" + qs.stringify vals
    http.get url, (resp) ->
      text = '';
      resp.on "data", (chunk) ->
        text += chunk

      resp.on "end", ->
        data = JSON.parse text
        console.log data
        if data.status is "reg"
          res.json {status: "ok"}
        else
          res.json {status: "errors", errors: data}