// Generated by CoffeeScript 1.7.1
(function() {
  var User, config, http;

  config = require("../config");

  http = require("http");

  User = require("../models/user");

  module.exports = function(req, res) {
    if (!req.cookies.runningId) {
      res.json(false);
      return false;
    }
    return User.findOne({
      hash: req.cookies.runningId
    }, function(err, user) {
      if (!user) {
        return res.json(false);
      } else {
        return http.get("http://adidas-running.ru/api/login?login=" + user.login + "&password=" + user.password, function(resp) {
          var text;
          text = '';
          resp.on("data", function(chunk) {
            return text += chunk;
          });
          return resp.on("end", function() {
            var data;
            data = JSON.parse(text);
            if (!data.auth) {
              res.json(false);
              return;
            }
            return res.json({
              instagram_username: user.instagramName,
              train: user.train
            });
          });
        });
      }
    });
  };

}).call(this);
