User = require "../models/user"
emailjs = require "emailjs"
jade = require "jade"
path = require "path"


module.exports = (req, res) ->
  User.findOne {hash: req.cookies.runningId}, (err, user) ->
    return res.json {status: "error"} unless user?

    user.train = extend user.train, req.body.data

    server = emailjs.server.connect
      user: "noreply@adidas-running.ru"
      password: "noreplypassword"
      host: "smtp.yandex.ru"
      ssl: true
      port: 465

    vars =
      mediaRoot: "http://energy.adidas-running.ru/img/"
      username: user.instagramName
      runclub: user.train.place

    data = jade.renderFile path.join(path.dirname(require.main.filename), "./views/server/confirmation.jade"), vars

    console.log data

    server.send
      text: data,
      from: "adidas-running <noreply@adidas-running.ru>"
      to: user.secure.login
      subject: "Подтверждение о выборе runclub>"
      attachment: [
        data: data
        alternative: true
      ]
    ,
    (err, mess) ->
      if err
        console.log err, "server.send"
        res.json {status: "error", error: err}
        return;

    user.save (err) ->
      if err
        console.log err, "user save"
        res.json {status: "error", error: err}
      else
        res.json {status: "ok"}
