config = require "../config"
http = require "http"
User = require "../models/user"

module.exports = (req, res) ->
  unless req.cookies.runningId
    res.json false
    return false;

  User.findOne {hash: req.cookies.runningId}, (err, user) ->
    unless user
      res.json false

    else
      http.get "http://adidas-running.ru/api/login?login=" + user.login + "&password=" + user.password, (resp)->
        text = '';
        resp.on "data", (chunk) ->
          text += chunk

        resp.on "end", ->
          data = JSON.parse text
          unless data.auth
            res.json false
            return;

          res.json {instagram_username: user.instagramName, train: user.train}