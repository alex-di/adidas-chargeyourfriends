fs = require "fs"
path = require "path"
rest = require "restler"

module.exports = (req, res) ->
  unless req.body.imagePath? && req.body.uploadUrl?
    res.send 502
    return

  p = path.join path.dirname(require.main.filename), "public", req.body.imagePath
  r = rest.post req.body.uploadUrl,
    multipart: true,
    data:
      'photo': rest.file p, null, fs.statSync(p).size, null, 'image/png'
  r.on 'complete', (data) ->
    console.log('restler', data);
    res.json data
