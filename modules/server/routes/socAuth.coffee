crypto = require "crypto"
User = require "../models/user"

module.exports = (req, res) ->
  console.log req.body.id
  hash = crypto.createHash('md5').update(req.body.id).digest('hex')

  User.findOne {runningId: req.body.id}, (err, user) ->
    console.log err if err

    unless user?
      user = new User
        runningId: req.body.id
        hash: hash
        secure: {
          login: req.body.email
        }
      user.save (err, item) ->
        res.cookie "runningId", hash
        res.json {status: "ok", user: user}

    else
      res.cookie "runningId", hash
      res.json {status: "ok", user: user}
