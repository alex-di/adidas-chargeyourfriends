// Generated by CoffeeScript 1.7.1
(function() {
  var config, crypto, http, qs, test;

  http = require("http");

  crypto = require("crypto");

  qs = require("querystring");

  config = require("../config");

  test = process.env.ENV === "dev";

  module.exports = function(req, res) {
    var errFields, errors, field, required, result, url, vals, _i, _len;
    vals = req.body;
    console.log(vals);
    result = {};
    required = ["agree", "bdate", "city", "password", "confirm", "email", "name"];
    if (vals.cartrunclub != null) {
      required.push("phone");
    }
    errors = [];
    errFields = {
      fields: [],
      type: "fields"
    };
    for (_i = 0, _len = required.length; _i < _len; _i++) {
      field = required[_i];
      if ((vals[field] == null) || vals[field] === "") {
        errFields.fields.push(field);
      }
    }
    if (errFields.fields.length > 0) {
      errors.push(errFields);
    }
    if (vals.password !== vals['confirm']) {
      errors.push({
        type: "pass"
      });
    }
    if (vals.sex != null) {
      vals.sex = vals.sex.toString();
    }
    if (errors.length > 0) {
      result.type = "errors";
      result.errors = errors;
      res.json(result);
      return console.log(errors);
    } else {
      result.status = "ok";
      console.log("no errors. get api req");
      vals.sig = crypto.createHash('md5').update(vals.email + "_" + vals.password + "_" + config.runningSecret).digest('hex');
      if (test) {
        vals.test = true;
      }
      url = "http://adidas-running.ru/api/reg?" + qs.stringify(vals);
      return http.get(url, function(resp) {
        var text;
        text = '';
        resp.on("data", function(chunk) {
          return text += chunk;
        });
        return resp.on("end", function() {
          var data;
          data = JSON.parse(text);
          console.log(data);
          if (data.status === "reg") {
            return res.json({
              status: "ok"
            });
          } else {
            return res.json({
              status: "errors",
              errors: data
            });
          }
        });
      });
    }
  };

}).call(this);
