User = require "../models/user"
http = require "http"
crypto = require "crypto"

module.exports = (req, res) ->
  unless req.body.login? or req.body.password? or req.body.id?
    res.send 500
    return;

  http.get "http://adidas-running.ru/api/login?login=" + req.body.login + "&password=" + req.body.password, (resp)->
    text = '';
    resp.on "data", (chunk) ->
      text += chunk

    resp.on "end", ->
      data = JSON.parse text

      unless data.auth or data.id is not req.body.id
        res.send 500
        return;
      hash = crypto.createHash('md5').update(data.id).digest('hex')
      res.cookie "runningId", hash

      User.findOne {runningId: data.id}, (err, user) ->
        console.log err if err

        unless user?
          user = new User
            runningId: data.id
            secure:
              login: req.body.login
              password: req.body.password
            hash: hash
            instagramId: data.instagram_id
            instagramUser: data.instagram_username

        user.save ->
          delete user.secure
          res.json {status: "ok", user: user}

