mongoose = require "mongoose"
Schema = mongoose.Schema

Charge = new Schema
  user: String
  type: {
    type: Number
    default: 0
  }
  sent:
    type: Boolean
    default: false

  sentDate: Date

Media = new Schema
  location:

    latitude:
      type: String
      default: 0

    longitude:
      type: String
      default: 0

  charging: [Charge]

  user: {
    id: Number
    profile_picture: String
    username: String
    full_name: String
  }

  link: String

  createdTime:
    type: Date
    default: Date.now

  instagramId: String
  images: {}

  last: String

  approved: {
    type: Number
    default: 0
  },
  { versionKey: false }

module.exports = mongoose.model "Media", Media