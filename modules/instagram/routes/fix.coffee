Media = require "../models/media"

get = (req, res) ->
  res.render "instagram/fix"

post = (req, res) ->
  unless req.body.inspirator && req.body.tofix && req.body.right
    res.redirect "/ig/fix"
    return
  Media.find {"user.username": req.body.inspirator, "charging.user": "@" + req.body.tofix}, (err, found) ->
    for item in found
      for charge in item.charging
        if charge.user is "@" + req.body.tofix
          charge.user = "@" + req.body.right
      item.save()
#    res.json found
  res.redirect "/ig/fix"

module.exports =
  get: get
  post: post