excel = require "excel-export"
Media = require "../models/media"
moment = require "moment"
fork = require("child_process").fork

module.exports = (req, res) ->
  conf = {cols: [], rows: []}
  conf.cols = [
    {type: "string", "caption": "Ник инспиратора", width: 16}
    {type: "string", "caption": "Дата когда запарсили фото", width: 14}
    {type: "string", "caption": "Ссылка на фото", width: 30}
    {type: "string", caption: "Ник отмечаемого (инспиратора или заряженного)", width: 16}
    {type: "string", "caption": "Дата, когда отметили", width: 14}
    {type: "string", caption: "Причина, если отмечаемого не отметили"}
  ]

  Media.find {}, (err, result) ->
    for item in result
      first = true

      for charge in item.charging
        sentDate = if charge.sent then moment(charge.sentDate).format("YYYY-MM-DD HH:mm") else "not sent"
        sentDate = item.createdTime unless sentDate?
        user = if first then item.user.username else ''
        cDate = if first then moment(item.createdTime).format("YYYY-MM-DD HH:mm") else ''
        link = if first then item.link else ""

        media = [user, cDate, link, charge.user, sentDate, "" ]
        conf.rows.push media
        first = false
      conf.rows.push ['', '', '', '', '', '']

    result = excel.execute conf
    res.setHeader 'Content-Type', 'application/vnd.openxmlformats'
    res.setHeader "Content-Disposition", "attachment; filename=" + "chargefriends-inspirations-export-" + moment().format('YYYY-MM-DD') + ".xlsx"
    res.end result, 'binary'