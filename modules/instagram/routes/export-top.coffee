excel = require "excel-export"
Media = require "../models/media"
moment = require "moment"

module.exports = (req, res) ->
  conf = {cols: [], rows: []}
  conf.cols = [
    {type: "string", "caption": "Ник инспиратора", width: 16}
    {type: "string", "caption": "Заряженных"}
  ]

  Media.find {}, (err, medias) ->
    console.log err if err
    pool = {}

    for media in medias
      if media.approved is 2
        for charge in media.charging
          unless charge.user is "@" || charge.user is "@" + media.user.username
            charge.user = charge.user.replace ".", ""
            pool[media.user.username] = {bound: media.user.username, user: media.user, charging:
              []} unless pool[media.user.username]?
            pool[media.user.username].charging.push charge.user.substr(1) if pool[media.user.username].charging.indexOf(charge.user.substr(1)) is -1


    for name, item of pool
      conf.rows.push [item.bound, item.charging.length]

    result = excel.execute conf
    res.setHeader 'Content-Type', 'application/vnd.openxmlformats'
    res.setHeader "Content-Disposition", "attachment; filename=" + "chargefriends-top-export-" + moment().format('YYYY-MM-DD') + ".xlsx"
    res.end result, 'binary'