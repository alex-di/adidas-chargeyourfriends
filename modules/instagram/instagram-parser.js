// Generated by CoffeeScript 1.7.1
(function() {
  var Media, child, config, ig, mongoose, path;

  path = require("path");

  Media = require("./models/media");

  mongoose = require("mongoose");

  child = require("child_process");

  ig = require("instagram-node").instagram();

  config = require("./config");

  ig.use(config.ig);

  module.exports = function(data) {
    var self;
    self = data.riot.observable(this);
    self.checkUser = function(username, cb) {
      var user;
      user = {
        inspirator: false,
        inspirated: false
      };
      return Media.find({
        "user.username": username
      }, function(err, result) {
        var item, _i, _len;
        for (_i = 0, _len = result.length; _i < _len; _i++) {
          item = result[_i];
          if (item.approved === 2) {
            user.inspirator = true;
          }
        }
        return Media.find({
          "charging": {
            $elemMatch: {
              user: "@" + username,
              "type": 0
            }
          }
        }, function(err, result) {
          var _j, _len1;
          console.log(result);
          for (_j = 0, _len1 = result.length; _j < _len1; _j++) {
            item = result[_j];
            if (item != null) {
              user.inspirated = true;
            }
          }
          return cb(user);
        });
      });
    };
    self.getUserData = function(name, cb) {
      return ig.user_search(name, {
        count: 1
      }, function(err, users) {
        if (err) {
          console.log(err, name);
        }
        if (users != null) {
          if (cb) {
            return cb(users[0] || false);
          }
        }
      });
    };
    self.checkTag = function(tag) {
      var opts;
      console.log("Checking tag " + tag);
      opts = {};
      return Media.find({}).select('last').limit(1).sort({
        $natural: -1
      }).exec(function(err, data) {
        if (err) {
          throw err;
        }
        if (data.length) {
          opts.min_tag_id = data[0].last;
        }
        return ig.tag_media_recent(tag, opts, function(err, medias, pagination, limit) {
          var charging, item, items, last, matches, media, user, _i, _len;
          if (err) {
            console.log(err);
            return;
          }
          last = pagination.min_tag_id;
          items = [];
          for (_i = 0, _len = medias.length; _i < _len; _i++) {
            media = medias[_i];
            matches = media.caption != null ? media.caption.text.match(/@[A-Za-z0-9\.\_]*/ig) : [];
            if (matches != null) {
              charging = (function() {
                var _j, _len1, _results;
                _results = [];
                for (_j = 0, _len1 = matches.length; _j < _len1; _j++) {
                  user = matches[_j];
                  _results.push({
                    user: user
                  });
                }
                return _results;
              })();
            } else {
              charging = [];
            }
            charging.unshift({
              user: "@" + media.user.username,
              type: 1
            });
            console.log(charging);
            item = new Media({
              last: last,
              instagramId: media.id,
              location: media.location,
              images: media.images,
              user: media.user,
              charging: charging,
              link: media.link
            });
            item.save();
            items.push(item);
          }
          return self.trigger("tagChecked", items);
        });
      });
    };
    self.on("load", function() {
      var redirect_uri;
      data.server.addRoute("get", "/ig", function(req, res) {
        return Media.count("_id", function(err, result) {
          return res.render("instagram/index", {
            styles: ["/css/insta.css"],
            scripts: require("../../config").rendering.scripts.concat(["/socket.io/socket.io.js", "/js/insta.js"]),
            pages: Math.ceil(result / 50)
          });
        });
      });
      data.server.addRoute("get", "/api/chargedby", function(req, res) {
        console.log(req.query);
        return Media.find({
          "charging": {
            $elemMatch: {
              user: "@" + req.query.name,
              type: 0
            }
          }
        }, function(err, medias) {
          var media, pool, _i, _len;
          if (err) {
            console.log(err);
          }
          pool = [];
          for (_i = 0, _len = medias.length; _i < _len; _i++) {
            media = medias[_i];
            if (pool.indexOf(media.user.username) === -1) {
              pool.push(media.user.username);
            }
          }
          return res.json(pool);
        });
      });
      data.server.addRoute("get", "/ig/listen", function(req, res) {
        return res.send(200, req.query['hub.challenge']);
      });
      data.server.addRoute("post", "/ig/listen", function(req, res) {
        var media, _i, _len, _ref;
        console.log("server: check tags");
        _ref = req.body;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          media = _ref[_i];
          self.checkTag(media.object_id);
        }
        return res.send(200);
      });
      redirect_uri = 'http://localhost:4013/api/ig-success';
      data.server.addRoute("get", "/api/ig-auth", function(req, res) {
        return res.redirect(ig.get_authorization_url(redirect_uri));
      });
      data.server.addRoute("get", "/api/ig-success", function(req, res) {
        return ig.authorize_user(req.query.code, redirect_uri, function(err, result) {
          if (err) {
            console.log(err);
            return res.json({
              status: "error",
              err: err
            });
          } else {
            data.server.updateUser(req.cookies.runningId, result);
            return res.render("instagram/success");
          }
        });
      });
      data.server.addRoute("get", "/ig/export", require("./routes/export"));
      data.server.addRoute("get", "/ig/top", require("./routes/export-top"));
      data.server.addRoute("get", "/ig/fix", require("./routes/fix").get);
      data.server.addRoute("post", "/ig/fix", require("./routes/fix").post);
      data.server.addRoute("get", "/ig/reparse", function(req, res) {
        self.reparse();
        return res.send(200);
      });
      data.server.addRoute("get", "/ig/cleanup", function(req, res) {
        self.cleanup();
        return res.send(200);
      });
      ig.add_tag_subscription(config.defaultTag, config.ig.listen, function(err, result, limit) {
        if (err) {
          return console.log(err);
        }
      });
      self.checkTag(config.defaultTag);
      return data.ig = self;
    });
    data.io.sockets.on("connection", function(socket) {
      socket.on("getPage", function(page) {
        var r;
        r = Media.find({});
        r.sort({
          createdTime: 1
        });
        r.limit(50);
        r.skip(50 * page);
        return r.exec(function(err, images) {
          var image, _i, _len, _results;
          _results = [];
          for (_i = 0, _len = images.length; _i < _len; _i++) {
            image = images[_i];
            _results.push(socket.emit("newImage", image));
          }
          return _results;
        });
      });
      socket.on("changedImage", function(image) {
        var id;
        console.log("socket: changed image", image);
        id = image._id;
        delete image._id;
        return Media.update({
          _id: id
        }, image, function(err, data) {
          if (err) {
            throw err;
          }
        });
      });
      return self.on("tagChecked", function(images) {
        return images.forEach(function(image) {
          return socket.emit("newImage", image);
        });
      });
    });
    self.reparse = function() {
      var all, call, found;
      found = 0;
      all = 0;
      call = function(err, medias, pagination, limit) {
        var charging, count, exp, item, last, matches, media, user, _i, _len;
        exp = true;
        count = 0;
        console.log(found, all);
        for (_i = 0, _len = medias.length; _i < _len; _i++) {
          media = medias[_i];
          all++;
          last = pagination.min_tag_id;
          matches = media.caption != null ? media.caption.text.match(/@[A-Za-z]*/ig) : null;
          if (matches != null) {
            charging = (function() {
              var _j, _len1, _results;
              _results = [];
              for (_j = 0, _len1 = matches.length; _j < _len1; _j++) {
                user = matches[_j];
                _results.push({
                  user: user
                });
              }
              return _results;
            })();
          } else {
            charging = [];
          }
          charging.unshift({
            user: "@" + media.user.username,
            type: 1
          });
          console.log(charging);
          item = new Media({
            last: last,
            instagramId: media.id,
            location: media.location,
            images: media.images,
            user: media.user,
            charging: charging,
            link: media.link,
            createdTime: media.created_time * 1000
          });
          console.log(item.link);
          item.save();
          if (media.created_time * 1 < 1403222400) {
            count++;
          }
          if (count > 10) {
            exp = false;
          }
        }
        if (pagination.next && exp) {
          return pagination.next(call);
        } else {
          console.log("end");
        }
      };
      return ig.tag_media_recent("зарядидрузей", {}, call);
    };
    self.cleanup = function() {
      return Media.find({}, function(err, medias) {
        var media, vals, _i, _len, _results;
        vals = [];
        _results = [];
        for (_i = 0, _len = medias.length; _i < _len; _i++) {
          media = medias[_i];
          if (vals.indexOf(media.link) > -1) {
            console.log("found " + media.link);
            _results.push(Media.remove({
              _id: media._id
            }).exec());
          } else {
            _results.push(vals.push(media.link));
          }
        }
        return _results;
      });
    };
    return self.trigger("load");
  };

}).call(this);
