path = require "path"
Media = require "./models/media"
mongoose = require "mongoose"
child = require "child_process"

ig = require "instagram-node"
.instagram()
config = require "./config"
ig.use config.ig

module.exports = (data) ->
  self = data.riot.observable(@)

  self.checkUser = (username, cb) ->
    user =
      inspirator: false
      inspirated: false
    Media.find {"user.username": username}, (err, result) ->
      for item in result
        user.inspirator = true if item.approved is 2
      Media.find {"charging": $elemMatch: {user: "@" + username, "type": 0}}, (err, result) ->
        console.log result
        for item in result
          user.inspirated = true if item?
        cb(user)


  self.getUserData = (name, cb) ->
    ig.user_search name, {count: 1}, (err, users) ->
      if err
        console.log err, name
      if users?
        cb(users[0] || false) if cb


  self.checkTag = (tag) ->
    console.log "Checking tag " + tag
    opts = {}
    Media.find({}).select 'last'
    .limit 1
      .sort {$natural: -1}
        .exec (err, data) ->
            throw err if err
            opts.min_tag_id = data[0].last if data.length
            ig.tag_media_recent tag, opts, (err, medias, pagination, limit) ->
              if err
                console.log err
                return

              last = pagination.min_tag_id
              items = [];
              for media in medias

                matches = if media.caption? then media.caption.text.match(/@[A-Za-z0-9\.\_]*/ig) else []
                if matches?
                  charging = for user in matches
                    user: user
                else charging = []

                charging.unshift
                  user: "@" + media.user.username
                  type: 1

                console.log charging
                item = new Media
                  last: last
                  instagramId: media.id
                  location: media.location
                  images: media.images
                  user: media.user
                  charging: charging
                  link: media.link

                item.save()
                items.push item

              self.trigger "tagChecked", items


  self.on "load", ->
    data.server.addRoute "get", "/ig", (req, res) ->

      Media.count "_id", (err, result) ->

        res.render "instagram/index",
          styles:
            ["/css/insta.css"]
          scripts:
            require "../../config"
            .rendering.scripts.concat [ "/socket.io/socket.io.js", "/js/insta.js"]
          pages: Math.ceil(result / 50)

    data.server.addRoute "get", "/api/chargedby", (req, res) ->
      console.log req.query
      Media.find {"charging": $elemMatch: {user: "@" + req.query.name, type: 0}}, (err, medias) ->
        console.log err if err
        pool = []
        for media in medias
          pool.push media.user.username if pool.indexOf(media.user.username ) is -1
        res.json pool


    data.server.addRoute "get", "/ig/listen", (req, res) ->
      res.send(200, req.query['hub.challenge'])

    data.server.addRoute "post", "/ig/listen", (req, res) ->
      console.log "server: check tags"
      for media in req.body
        self.checkTag media.object_id
      res.send 200

    redirect_uri = 'http://localhost:4013/api/ig-success';

    data.server.addRoute "get", "/api/ig-auth", (req, res) ->
      res.redirect ig.get_authorization_url redirect_uri

    data.server.addRoute "get", "/api/ig-success", (req, res) ->
      ig.authorize_user req.query.code, redirect_uri, (err, result) ->
        if (err)
          console.log err
          res.json {status: "error", err: err}
        else
          data.server.updateUser req.cookies.runningId, result
          res.render "instagram/success"

    data.server.addRoute "get", "/ig/export", require "./routes/export"
    data.server.addRoute "get", "/ig/top", require "./routes/export-top"
    data.server.addRoute "get", "/ig/fix", require("./routes/fix").get
    data.server.addRoute "post", "/ig/fix", require("./routes/fix").post
    data.server.addRoute "get", "/ig/reparse", (req, res) ->
      self.reparse()
      res.send 200

    data.server.addRoute "get", "/ig/cleanup", (req, res) ->
      self.cleanup()
      res.send 200



    ig.add_tag_subscription config.defaultTag, config.ig.listen, (err, result, limit) ->
      console.log err if err

    self.checkTag config.defaultTag
    data.ig = self;

  data.io.sockets.on "connection", (socket) ->
    socket.on "getPage", (page) ->
      r = Media.find {}
      r.sort
        createdTime: 1
      r.limit 50
      r.skip 50 * page
      r.exec (err, images) ->
        for image in images
          socket.emit "newImage", image

    socket.on "changedImage", (image) ->
      console.log "socket: changed image", image
      id = image._id
      delete image._id
      Media.update _id: id, image, (err, data) ->
        throw err if err

    self.on "tagChecked", (images) ->
      images.forEach (image) ->
        socket.emit "newImage", image

  self.reparse = ->
    found = 0
    all = 0
    call = (err, medias, pagination, limit) ->
      exp = true
      count = 0
      console.log found, all
      for media in medias
        all++

        last = pagination.min_tag_id
        matches = if media.caption? then media.caption.text.match(/@[A-Za-z]*/ig) else null
        if matches?
          charging = for user in matches
            user: user
        else charging = []

        charging.unshift
          user: "@" + media.user.username
          type: 1

        console.log charging
        item = new Media
          last: last
          instagramId: media.id
          location: media.location
          images: media.images
          user: media.user
          charging: charging
          link: media.link
          createdTime: media.created_time * 1000


        console.log item.link
        item.save()

        if media.created_time * 1 < 1403222400
          count++


        exp = false if count > 10


      if pagination.next and exp
        pagination.next call
      else
        console.log "end"
        return;


    ig.tag_media_recent "зарядидрузей", {}, call

  self.cleanup = ->
    Media.find {}, (err, medias) ->
      vals = []
      for media in medias
        if vals.indexOf(media.link) > -1
          console.log "found " + media.link
          Media.remove(_id: media._id).exec()

        else
          vals.push media.link



  self.trigger "load"