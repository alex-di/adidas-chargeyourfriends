path = require "path"
cluster = require "cluster"
images = require "images"
http = require "http"
fs = require "fs"

parseImage = (el, cb) ->
  http.get el.link, (res) ->
    image = ""
    res.setEncoding('binary')

    res.on "error", (err)->
      console.log err
    res.on "data", (chunk) ->
      image += chunk
    res.on "end", ->
#      image = new Buffer image, "binary"
#      out = images 900, 900
#      out.draw images(image), 370, 417
#      out.draw images(path.join(__dirname, "stuff/inspirator.png")), 0, 0
#      out.save path.join __dirname, "../../public/share/" + el.user + "-inspirator.jpg"
#
#      out = images 900, 900
#      out.draw images(image), 495, 614
#      out.draw images(path.join(__dirname, "stuff/inspirated.png")), 0, 0
#      out.save path.join __dirname, "../../public/share/" + el.user + "-inspirated.jpg"

      cb()

if cluster.isMaster
  mongoose = require "mongoose"
  config = require "../../config"
  mongoose.connect config.db
  Avatar = require path.join __dirname, "./models/avatar"

  handler = (arr) ->
    if el = arr.pop()
      console.log el
      if el.user?
        setTimeout ->
          if el.link?
            cluster.fork()
            .on "message", (mess) ->
                proc = this
                if mess
                  Avatar.findByIdAndUpdate mess, {$set:{share:true}}, (err, item)->
                    console.log err if err
                    console.log mess
                    proc.kill()
            .send el

          handler(arr)
        , 3000

  Avatar.find {share: null}, (err, unshared)->
    console.log err if err
    handler unshared



else
  process.on "message", (el)->
    parseImage el, ->
      process.send el._id