Bound = mongoose = require "mongoose"
Schema = mongoose.Schema

Avatar = require "./models/avatar"

Media = require "./models/media"

child = require "child_process"
path = require "path"

saveAll = (all, result, total, cb) ->
  key = Object.keys(all)[0]
  doc = all[key]
  delete all[key]
  doc.save (err, saved) ->
    console.log err if err
    result.push saved
    if --total
      saveAll all, result, total, cb
    else cb(result) if cb?

module.exports = (data) ->

  Bound = new Schema
    bound: String
    avatar: String
    user: {}
    charging: []

  Bound.pre "save", (next) ->
    self = @
    Avatar.findOne {user: self.bound}, (err, avt) ->
      unless avt? && avt
        data.ig.getUserData self.bound, (user) ->
          console.log "Get avatar 4 " + self.bound
          av = new Avatar
            user: self.bound
            link: user.profile_picture
          av.save()
          console.log av
          self.avatar = user.profile_picture
          next()
      else
        self.avatar = avt.link
        next()

  Bound = mongoose.model "Bound", Bound

  self = data.riot.observable(@)
  self.bounds = []
  self.generate = (cb) ->
    Media.find {}, (err, medias) ->
      console.log err if err
      pool = {}

      for media in medias
        if media.approved is 2
          for charge in media.charging
            unless charge.user is "@" || charge.user is "@" + media.user.username
              charge.user = charge.user.replace ".", ""
              pool[media.user.username] = new Bound {bound: media.user.username, user: media.user, charging:
                []} unless pool[media.user.username]?
              pool[media.user.username].charging.push charge.user.substr(1) if pool[media.user.username].charging.indexOf(charge.user.substr(1)) is -1
              unless pool[charge.user.substr(1)]
                pool[charge.user.substr(1)] = new Bound {bound: charge.user.substr(1), user: media.user, charging: []}

      saveAll pool, [], Object.keys(pool).length, cb


  self.workflow = ->
    console.log "Bound generator started"
    share = child.fork path.join __dirname, "./instagram-share.js"
    Bound.remove {}, (err) ->
      console.log err if err
      self.generate (result) ->
        self.bounds = result
        self.trigger "update"
        console.log "Bound generator workflow at " + Date()

  unless data.env.ENV is "dev"
    setInterval ->
      self.workflow()
    , 60 * 60 * 60 *  24
  self.workflow()

  Bound.find {}, (err, res) ->
    console.log err if err
    self.bounds = res

  data.bounder = self

  return self