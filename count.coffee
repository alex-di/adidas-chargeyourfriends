config = require "./config"
mongoose = require "mongoose"
mongoose.connect config.db
Media = require "./modules/instagram/models/media"

Media.find {}, (err, medias) ->
  charges = []
  chargesUnic = []
  for media in medias
    for charge, i in media.charging
      unless i is 0
        charges.push charge.user
        if chargesUnic.indexOf(charge.user) is -1
          chargesUnic.push charge.user
  console.log charges.length, chargesUnic.length
