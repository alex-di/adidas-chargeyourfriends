'use strict';

module.exports = function(grunt) {

    require('jit-grunt')(grunt);
    require('time-grunt')(grunt);

    grunt.initConfig({

        newer: {
            options: {
                override: function(details, include) {
                    include(true);
                }
            }
        },

        sass: {
            options: {
                sourcemap: false
            },
            dist: {
                files: {
                    '../css/mobile.css': 'scss/style.scss'
                }
            }
        },

        autoprefixer: {
            options: {
                browsers: ['last 2 versions', 'ie 8', 'ie 9']
            },
            dist: {
                files: {
                    '../css/mobile.css': '../css/mobile.css'
                }
            }
        },

        concat: {
            dist: {
                src: [
                    '../js/vendor/chosen.jquery.min.js',
                    '../js/base.mobile.js'
                ],
                dest: '../js/running.mobile.min.js'
            }
        },

        uglify: {
            build: {
                src: '../js/running.mobile.min.js',
                dest: '../js/running.mobile.min.js'
            }
        },

        csso: {
            dynamic_mappings: {
                expand: true,
                cwd: '../project/css/',
                src: ['*.css', '!*.min.css'],
                dest: '../project/css/',
                ext: '.min.css'
            }
        },

        jade: {
            compile: {
                options: {
                    pretty: true,
                    data: {
                        debug: true
                    }
                },
                files: {
                    "html/1.html": "../../views/phone/1.jade",
                    "html/2.html": "../../views/phone/2.jade",
                    "html/3.html": "../../views/phone/3.jade",
                    "html/4.html": "../../views/phone/4.jade",
                    "html/5.html": "../../views/phone/5.jade",
                    "html/6.html": "../../views/phone/6.jade",
                    "html/7.html": "../../views/phone/7.jade",
                    "html/8.html": "../../views/phone/8.jade",
                    "html/9.html": "../../views/phone/9.jade"
                }
            }
        },

        watch: {
            options: {
                livereload: true,
            },
            sass: {
                options: {
                    livereload: false,
                    sourcemap: true
                },
                files: ['scss/**/*.scss'],
                tasks: ['newer:sass:dist', 'autoprefixer'],
            },
            jade: {
                files: ['templates/*.jade'],
                tasks: ['newer:jade:compile']
            }
        },
    });

    grunt.registerTask('release', ['sass:dist', 'autoprefixer', 'csso', 'concat', 'uglify']);
    grunt.registerTask('default', ['watch']);

};