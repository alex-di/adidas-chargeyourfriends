!function ($) {
    
    $(function () {

        'use strict';

        window.Modals = {}

        var Modals = (function() {

            Modals.prototype.bindEvents = function() {
                var that = this;
                
                $('.js-modalClose').on('click', {modal: this}, this.hide);
                $('.js-modalOpen').on('click', {modal: this}, this.show);
                $('.overlay').on('click', {modal: this}, this.hide);
                
                $(document).on('keyup', function(e){
                    if (e.keyCode == that.escKey) {
                        that.hide({data: { modal: that}});
                    }
                });
            };

            Modals.prototype.cacheElements = function() {
                this.$allPopups = $('.modal');
                this.$overlay = $('.overlay');
            };

            Modals.prototype.show = function(e) {
                e.preventDefault();

                var that = e.data.modal,
                    idModal = $(this).data('modal'),
                    href = $(this).attr('href');
                
                if (idModal) {
                    that.hideAll();

                    that.$popupBlock = $('#' + idModal);
                    that._show(idModal);
                }
            };

            Modals.prototype.forceShow = function(idModal, callback) {
                this.$popupBlock = $('#' + idModal);
                if (this.$popupBlock.length !== 0) {
                    this._show(idModal);

                    if (typeof callback !== 'undefined' && typeof callback == 'function') {
                        callback();
                    }
                }
            };

            Modals.prototype.hide = function(e) {
                var that = e.data.modal;
                if (typeof that.$popupBlock !== 'undefined') {
                    that.$popupBlock.fadeOut(250, function(){
                        that.$overlay.hide();
                        that.$allPopups.hide();
                        $('body').css('overflow', 'auto');
                    });
                }
            };

            Modals.prototype.hideAll = function() {
                this.$overlay.hide();
                this.$allPopups.hide();
            };

            Modals.prototype.forceHide = function(idModal, callback) {
                this.$popupBlock = $('#' + idModal);
                if (this.$popupBlock.length !== 0) {
                    this.$overlay.fadeOut(250);
                    this.$popupBlock.fadeOut(250);

                    if (typeof callback !== 'undefined' && typeof callback == 'function') {
                        callback();
                    }
                } else {
                    return false;
                }
            };



            Modals.prototype._show = function(idModal) {                
                this.$overlay.show();
                this.$popupBlock.fadeIn(250);
                $('body').css('overflow', 'hidden');
            };

            function Modals(popupId) {
                this.escKey = 27;
                this.cacheElements();
                this.bindEvents();
            };

            return Modals;

        })();

        $(document).ready(function(){

            var isMacLike = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i) ? true : false;
            if (isMacLike === true) {
                $('html').addClass('mac');
            }

            window.Modals.instance = new Modals();

            Modernizr.load({
                test: Modernizr.input.placeholder,
                nope: "js/fallbacks/jquery.placeholder.js"
            });

            var testEl = document.createElement( "x-test" ),
                supportsWebkitBackgroundClipText = typeof testEl.style.webkitBackgroundClip !== "undefined" && ( testEl.style.webkitBackgroundClip = "text", testEl.style.webkitBackgroundClip === "text" );


            if (supportsWebkitBackgroundClipText !== true) {
                $('html').addClass('no-backgroundClipText');
            }

            if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i)
                || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
                
                $(window).on('orientationchange', function(){
                    switch (window.orientation) {  
                        case -90:
                        case 90:
                            $('.overlay-black').hide();
                            $('.rotate').fadeOut(250);
                        break; 
                        default:
                            $('.overlay-black').show();
                            $('.rotate').fadeIn(250);
                        break;
                    }
                });

                if (!(window.orientation == 90 || window.orientation == -90)) {
                    $('.overlay-black').show();
                    $('.rotate').fadeIn(250);
                }
            }


            $('.js-instagramFind').on('click', function(){
                $(this).hide();
                $('.js-instagramSignin').show();
            });

            $('.form__customSelect').chosen({
                inherit_select_classes: true
            });

            $('.js-validateForm').on('submit', function(e){
                var $validate = $(this).find('.js-validate'),
                    isError = false;

                $(this).find('.invalid').removeClass('invalid');
                $(this).find('.js-invalidMessage').html('');
                
                $($validate).each(function(){

                    var value = $.trim($(this).val()),
                        $parent = $(this).parent(),
                        data = $(this).data(),
                        isRequired = (data.required == true || $(this).attr('required') == true) ? true : false,
                        isValid = false,
                        minLength = (data.minlength || $(this).attr('minlength')),
                        maxLength = (data.maxlength || $(this).attr('maxlength')),
                        defaultValue = $(this).attr('placeholder') || '';
                    
                    if (!$($parent).hasClass('hidden')) {

                        if (isRequired) {
                            if (value == '' || value == defaultValue) {
                                $($parent).addClass('invalid')
                                          .find('.js-invalidMessage')
                                          .html('Поле обязательно для заполнения');
                                isError = true;
                            }
                        }

                        if (typeof minLength !== 'undefined') {
                            isValid = (value.length < minLength) ? true : false;

                            if (isValid === false) {
                                $($parent).addClass('invalid')
                                          .find('.js-invalidMessage')
                                          .html('Значение поля слишком короткое');

                                isError = true;
                            }   
                        }

                        if (typeof maxLength !== 'undefined') {
                            isValid = (value.length < maxLength) ? true : false;

                            if (isValid === false) {
                                $($parent).addClass('invalid')
                                          .find('.js-invalidMessage')
                                          .html('Значение поля слишком длинное');
                                isError = true;
                            }      
                        }

                        if (typeof data.validator !== 'undefined' && !(value == '' || value == defaultValue)) {
                            switch(data.validator) {
                                case 'number':
                                    var regex = /\d+/;
                                    if (!regex.test(value)) {
                                        $($parent).addClass('invalid');
                                        isError = true;
                                    }
                                break;
                                case 'name':
                                    var regex = /^[A-Za-zА-Яа-я ]+$/;
                                    if (!regex.test(value)) {
                                        $($parent);
                                        isError = true;
                                        $($parent).addClass('invalid')
                                                  .find('.js-invalidMessage')
                                                  .html('В поле ФИО могут быть только буквы и пробел');
                                    }
                                break;
                                case 'email':
                                    var lastAtPos = value.lastIndexOf('@'),
                                        lastDotPos = value.lastIndexOf('.'),
                                        isValid = (lastAtPos < lastDotPos && lastAtPos > 0 && value.indexOf('@@') == -1 && lastDotPos > 2 && (value.length - lastDotPos) > 2) ? true : false;

                                    if(data.used)
                                        isValid = false
                                    console.log(data.used)

                                    if (isValid === false) {
                                        $($parent).addClass('invalid')
                                                  .find('.js-invalidMessage')
                                                  .html(data.used ? "Данный email был использован" : 'Неверный формат email');

                                        isError = true;
                                    }
                                break;
                            }
                        }
                    }
                });

                if (isError) {
                    e.preventDefault();
                    return false;
                }
            });
        
            
        });
    });

    $(".js-validate[data-validator='email']").on("change", function(){
        var s = $(this)

        $.ajax("/used-email", {data: {email: s.val()}})
            .done(function(res){
                console.log(res)
                if(res)
                    s.data("used", false)
                else
                    s.data("used", true)

            })
        s.removeAttr("data-used")

    })

    $(".js-validate[data-validator='email']").on("keyup", function(){
        var s = $(this)

        $.ajax("/used-email", {data: {email: s.val()}})
            .done(function(res){
                console.log(res)
                if(res)
                    s.removeAttr("data-used")
                else
                    s.attr("data-used", true)

            })
        s.removeAttr("data-used")

    })

}(window.jQuery);   