random = (min, max) ->
  Math.floor(Math.random() * (max - min + 1)) + min;

VK.init({apiId: 4483901});
userId = 0;


Bounder = (p) ->
  assoc = []
  for bound, key in document.bounds
    assoc[key] = bound.bound
  self = riot.observable @

  self.r = d3.select("#graph").append("svg:svg")
  .attr
      width: p.dim.w
      height: p.dim.h

  self.n = []
  self.l = []
  self.f = d3.layout.force()
  self.d = self.r.append "svg:defs"

  self.start = ->
    self.trigger "ready"

  self.addNode = (node) ->
    if node.x && node.y
      node.fixed = true
    i = self.n.push node
    return i - 1

  self.addLink = (link) ->
    i = self.l.push link
    return i - 1

  self.r.on "mousemove", ->
    mouse = d3.mouse @
  #    console.log mouse

  self.on "ready", ->
    self.f.links self.l
    .nodes self.n

    self.f.start()

    self.d.append "pattern"
    .attr
        "id": "closer"
        x: 0
        y: 0
        width: 40
        height: 40
    .append "image"
      .attr
          x: 0
          y: 0
          width: 40
          height: 40
          "xlink:href": "/img/graph-closer.png"
    self.d.append "pattern"
    .attr
        "id": "linker"
        x: 0
        y: 0
        width: 40
        height: 40
    .append "image"
      .attr
          x: 0
          y: 0
          width: 40
          height: 40
          "xlink:href": "/img/graph-linker.png"

    self.d.append "pattern"
    .attr
        "id": "social-fb"
        x: 0
        y: 0
        width: 40
        height: 40
    .append "image"
      .attr
          x: 0
          y: 0
          width: 40
          height: 40
          "xlink:href": "/img/social-fb.png"

    self.d.append "pattern"
    .attr
        "id": "social-vk"
        x: 0
        y: 0
        width: 40
        height: 40
    .append "image"
      .attr
          x: 0
          y: 0
          width: 40
          height: 40
          "xlink:href": "/img/social-vk.png"

    self.d.append "pattern"
    .attr
        "id": "social-tw"
        x: 0
        y: 0
        width: 40
        height: 40
    .append "image"
      .attr
          x: 0
          y: 0
          width: 40
          height: 40
          "xlink:href": "/img/social-tw.png"

    self.d.append "pattern"
    .attr
        "id": "ligthning"
        x: 0
        y: 0
        width: 31
        height: 31
    .append "image"
      .attr
          x: 0
          y: 0
          width: 31
          height: 32
          "xlink:href": "/img/lightning.jpg"

    self.d.append "pattern"
    .attr
        "id": "ligthning2"
        x: 0
        y: 0
        width: 20
        height: 20
    .append "image"
      .attr
          x: 0
          y: 0
          width: 20
          height: 20
          "xlink:href": "/img/lightning2.png"

    links = self.r.selectAll "line"
    .data self.l
      .enter()
      .append "line"
        .attr "x1", (d) ->
            d.source.x
        .attr "y1", (d) ->
            d.source.y
        .attr "x2", (d) ->
            d.target.x
        .attr "y2", (d) ->
            d.target.y
        .style "stroke-width", (d) ->
            d.width || 1
        .style "stroke", (d) ->
            if d.source.style.fill? || d.target.style.fill
              unless d.source.style.fill?
                return d.target.style.fill
              unless d.target.style.fill?
                return d.source.style.fill
              if d.source.style.fill == d.target.style.fill
                return d.source.style.fill
              else
                if self.d.select("#grad" + d.source.style.fill.substr(1) + d.target.style.fill.substr(1)).size() is 0
                  gradient = self.d.append "svg:linearGradient"
                  .attr "id", "grad" + d.source.style.fill.substr(1) + d.target.style.fill.substr(1)
                    .attr("x1", "0%")
                    .attr("y1", "0%")
                    .attr("x2", "100%")
                    .attr("y2", "100%")

                  gradient.append "svg:stop"
                  .attr("offset", "0%")
                  .attr("stop-color", d.source.style.fill)
                  .attr("stop-opacity", 1)

                  gradient.append("svg:stop")
                  .attr("offset", "100%")
                  .attr("stop-color", d.target.style.fill)

                return "url(#grad" + d.source.style.fill.substr(1) + d.target.style.fill.substr(1) + ")"
    nodes = self.r.selectAll ".bg"
    .data self.n
      .enter()
      .append "circle"
        .attr
            class: "bg"
            cx: (d) ->
              d.x
            cy: (d) ->
              d.y
            r: (d) ->
              if d.r is 0 then 0 else d.r + 3
        .style
            opacity: 0.2
            fill: (d) ->
              return "none" unless d.style?
              d.style.fill || "none"
    nodes = self.r.selectAll ".main"
    .data self.n
      .enter()
      .append "circle"
        .attr
            class: "main"
            cx: (d) ->
              d.x
            cy: (d) ->
              d.y
            r: (d) ->
              d.r
        .style
            fill: (d) ->
              return "none" unless d.style?
              d.style.fill || "none"
        .on "mouseover", (d) ->
            d3.select(@).transition().attr "r", d.r + 7
        .on "mouseout", (d) ->
            d3.select(@).transition().attr "r", d.r
        .on "click", (d) ->
            _gaq.push(['_trackEvent','userBeh', 'click', 'point']);
            self.showPpl({x: d.x, y: d.y})

    self.createShoe()
    self.trigger "loaded"

    all = self.r.selectAll ".main"
    .filter (d) ->
        d.r > 0

    setInterval ->
      all.sort ->
        random(-1, 1)
      i = all[0][0]
      i = d3.select(i)
      i.transition().duration(500).attr "r", (d) ->
        d.r + 5
      .transition().duration(500).attr "r", (d) ->
        d.r
    , 1000


  self.createShoe = ->
    self.r.append "circle"
    .attr
        "cx": 253,
        "cy": 246,
        "r": 70,
        class: "shoe-bg"
    .style
        "fill": "#ffe111"
    self.r.append "circle"
    .attr
        "cx": 253,
        "cy": 246,
        "r": 80,
        class: "shoe-bg"
    .style
        "fill": "#ffe111"
        opacity: 0.2

    shoe = self.r.append "image"
    .attr
        x: 188
        y: 202
        width: 154
        height: 74
        "xlink:href": "/img/shoe.png"
        class: "shoe-img"


    .on "click", ->
        window.Modals.instance.forceShow('modalProduct')

    .on "mouseout", ->
        shoe
        .transition()
        .attr
            "width": 154
            height: 74
            x: 188
            y: 202

        self.r.selectAll ".shoe-text"
        .transition()
        .style "opacity", 0
          .remove()
    .on "mouseover", ->
        shoe
        .transition()
        .attr
            "width": 200
            height: 96
            x: 167
            y: 190
        self.r.append "text"
        .text "об энергии boost >"
          .attr
              x: 188
              y: 360
              class: "shoe-text"
              "text-anchor": "left"
              width: 100
          .style
              "font-size": 16
              "max-width": 100
              fill: "#e8196a"
              opacity: 0
          .transition()
          .style
              opacity: 1

        self.r.append "text"
        .text "узнай больше"
          .attr
              x: 188
              y: 340
              class: "shoe-text"
              "text-anchor": "left"
              width: 100
          .style
              "font-size": 16
              "max-width": 100
              fill: "#e8196a"
              opacity: 0
          .transition()
          .style "opacity", 1


  socials = [
    {name: "tw"}
    {name: "fb"}
    {name: "vk"}
  ]

  document.Share =
#      vk: (purl, ptitle, pimg, text) ->
    vk: (data)->
      VK.Auth.login (response) ->
        if response.session
          userId = response.session.user.id;
          VK.api 'photos.getWallUploadServer', {'user_id': userId }, (data) ->
            if (data.response)
              data = data.response
              uploadUrl = data.upload_url
              console.log self.data
              $.post '/upload',
                uploadUrl: uploadUrl,
                imagePath: "share/" + location.hash.slice(1) + (if self.data.inspirator then "-inspirator" else "-inspirated") + ".jpg",
              , (json) ->
                json = JSON.parse json
                console.log json
                if json.server
                  VK.api 'photos.saveWallPhoto', {
                    'user_id': userId,
                    'server': json.server,
                    'photo': json.photo,
                    'hash': json.hash
                  }, (data) ->
                    console.log data
                    if (data.response)
                      VK.api "wall.post", {owner_id: userId, message: "Я заряжаю друзей энергией бега! #зарядидрузей #boost #adidasrunning", attachments: data.response[0].id}, (data) ->
                        console.log "wpend"
                else
                  "json server fault"

      , 8199

    tw: ->
      this.popup "http://twitter.com/share?url=" + encodeURIComponent(location.href.replace(/#/,
        "user/")) + "&text=Я заряжаю друзей энергией бега!&hashtags=зарядидрузей,boost,adidasrunning"

    fb: ->
      this.popup "http://www.facebook.com/sharer.php?u=" + encodeURIComponent(location.href.replace(/#/, "user/"))

    popup: (url) ->
      window.open(url, '', 'toolbar=0,status=0,width=626,height=436');


  collect = (name, depth, pool) ->
    console.log "Collect", name
    pool = [] unless pool
    depth = 0 unless depth?

    i = assoc.indexOf name
    if i is -1
      return false
    pool.push name
    result = document.bounds[i]
    result.links = []
    result.depth = depth
    result.inspirators = false
    ++depth


    if document.bounds[i].charging.length is 0
      if depth is 1
        result.inspirators = []

    if depth < 3
      unless document.bounds[i].charging.length is 0

        if depth > 1
          unless document.bounds[i].charging[0] is name || pool.indexOf(document.bounds[i].charging[0]) > -1
            result.links = result.links.concat collect document.bounds[i].charging[0], depth, pool
        else
          for runner, i in document.bounds[i].charging
            unless runner is name || pool.indexOf(runner) > -1
              result.links = result.links.concat collect runner, depth, pool
    return result

  toggleSoc = (coords) ->
    if self.r.selectAll(".social").size() is 0
      for net, i in socials
        self.r.selectAll(".tip-text.main").transition().style "opacity", 0
        self.r.append "circle"
        .attr
            class: "social"
            id: "social-" + net.name
            cx: coords.x + 45
            cy: ->
              if coords.y < 250
                coords.y + 105
              else
                coords.y - 115
            r: 20
            fill: "url(#social-" + net.name + ")"
            link: net.name
        .on "click", ->
            document.Share[d3.select(this).attr("link")]()
        .transition()
        .attr "cx", coords.x + 45 - ((i + 1) * 45)
    else
      self.r.selectAll(".tip-text.main").transition().style "opacity", 1
      self.r.selectAll(".social").transition()
      .attr "cx", coords.x + 45
        .remove()

  generate = (runner, coords, pool) ->
    console.log "generate", runner.bound, runner.depth, 430
    coords.x = 250 if coords.x < 250
    coords.x = 1050 if coords.x > 1050
    coords.y = 200 if coords.y < 200
    coords.y = 610 if coords.y > 610
    pool = [] unless pool
    pool.push runner.bound
    faces = d3.selectAll(".face")
    for r, i in runner.links
      if pool.indexOf(r.bound) is -1 && i < 7

        match = false
        while !match
          crds =
            x: random(200, 1000)
            y: random(200, 500)
          match = true
          rng = Math.abs(Math.sqrt((coords.x - crds.x) * (coords.x - crds.x) + (coords.y - crds.y) * (coords.y - crds.y)))
          console.log rng
          if rng < 150
            match = false

          faces.each ->
            d = d3.select(this)
            rng = Math.abs(Math.sqrt((crds.x - d.attr("cx")) * (crds.x - d.attr("cx")) + (crds.y - d.attr("cy")) * (crds.y - d.attr("cy"))))
            if rng < 200
              match = false

        self.r.append "line"
        .attr
            x1: coords.x
            x2: crds.x
            y1: coords.y
            y2: crds.y
            class: "faceline"
        .style
            "stroke-width": 3
            stroke: "#fedc00"
        generate r, crds, pool

    if runner.inspirators
      $.get "/api/chargedby", {name: runner.bound}
      .done (inspirators) ->
          for insp in inspirators
            i = assoc.indexOf insp
            itm = document.bounds[i]
            unless i is -1
              faces = d3.selectAll(".face")

              match = false
              while !match
                crds =
                  x: random(200, 1000)
                  y: random(200, 500)
                match = true
                rng = Math.abs(Math.sqrt((coords.x - crds.x) * (coords.x - crds.x) + (coords.y - crds.y) * (coords.y - crds.y)))
                if rng < 200
                  match = false

              faces.each ->
                d = d3.select(this)
                rng = Math.abs(Math.sqrt((crds.x - d.attr("cx")) * (crds.x - d.attr("cx")) + (crds.y - d.attr("cy")) * (crds.y - d.attr("cy"))))
                if rng < 200
                  match = false

              self.r.append "line"
              .attr
                  x1: coords.x
                  x2: crds.x
                  y1: coords.y
                  y2: crds.y
                  class: "faceline"
              .style
                  "stroke-width": 3
                  stroke: "#f67c34"

              self.r.append "circle"
              .attr
                  class: "face-bg"
                  cx: crds.x
                  cy: crds.y
                  r: 0
              .style
                  fill: "#f67c34"
                  opacity: .2
              .transition()
              .attr "r", 70

              self.r.append "circle"
              .attr
                  class: "face"
                  cx: crds.x
                  cy: crds.y
                  r: 0
              .style
                  fill: ->
                    if self.d.selectAll("#image" + itm.bound).size() is 0
                      image = self.d.append "pattern"
                      .attr
                          "id": "image" + itm.bound
                          x: 0
                          y: 0
                          width: 1
                          height: 1
                      .append "image"
                        .attr
                            x: 0
                            y: 0
                            width: 150
                            height: 150
                            "xlink:href": itm.avatar
                    "url(#image" + itm.bound + ")"

                  stroke: "#f67c34"
                  "stroke-width": 10
              .transition()
              .attr "r", 50

              self.r.append "text"
              .text "@" + itm.bound
                .attr
                    x: ->
                      if itm.depth is 0
                        crds.x + 60 - this.getComputedTextLength()
                      else
                        crds.x
                    y: ->
                      crds.y + 70 - 15 + 30 - 7
                    "text-anchor": "middle"
                    class: "tip-text " + if itm.depth is 0 then "main" else ''
                .style
                    "font-size": 24

          self.r.append "circle"
          .attr
              class: "face-bg"
              cx: coords.x
              cy: coords.y
              r: 0
          .style
              fill: if runner.depth is 0 then "#d6389c" else "#fedc00"
              opacity: .2
          .transition()
          .attr "r", (70 - runner.depth * 15) + 20 - 7 * runner.depth

          self.r.append "circle"
          .attr
              class: "face"
              cx: coords.x
              cy: coords.y
              r: 0
          .style
              stroke: if runner.depth is 0 then "#d6389c" else "#fedc00"
              "stroke-width": 10
              "fill": ->
                if self.d.selectAll("#image" + runner.bound).size() is 0
                  image = self.d.append "pattern"
                  .attr
                      "id": "image" + runner.bound
                      x: 0
                      y: 0
                      width: 1
                      height: 1
                  .append "image"
                    .attr
                        x: 0
                        y: 0
                        width: 150
                        height: 150
                        "xlink:href": runner.avatar
                "url(#image" + runner.bound + ")"
          .transition()
          .attr "r", 70 - runner.depth * 15


          if runner.depth is 0
            self.r.append "circle"
            .attr
                class: "face-close"
                cx: coords.x + 90
                cy: ->
                  if coords.y < 250
                    coords.y + 105
                  else
                    coords.y - 115
                r: 0
                fill: "url(#closer)"
            .on "click", ->
                self.closePpl()

            .transition()
            .attr "r", 20

            self.r.append "circle"
            .attr
                class: "face-link"
                cx: coords.x + 45
                cy: ->
                  if coords.y < 250
                    coords.y + 105
                  else
                    coords.y - 115
                r: 20
                fill: "url(#linker)"
            .on "click", ->
                toggleSoc(coords)
            .transition()
            .attr "r", 20

          self.r.append "text"
          .text "@" + runner.bound
            .attr
                x: ->
                  if runner.depth is 0
                    coords.x + 20 - this.getComputedTextLength()
                  else
                    coords.x
                y: ->
                  if runner.depth is 0
                    if coords.y < 250
                      coords.y + 190 - ((70 - runner.depth * 15) + 7 - 7 * runner.depth)
                    else
                      coords.y - 30 - ((70 - runner.depth * 15) + 7 - 7 * runner.depth)
                  else
                    coords.y + ((70 - runner.depth * 15) + 30 - 7 * runner.depth)
                "text-anchor": "middle"
                class: "tip-text " + if runner.depth is 0 then "main" else ''
            .style
                "font-size": if runner.depth is 0 then 30 else 24
          self.r.append "circle"
          .attr
              class: "ligthning"
              cx: (coords.x + crds.x) / 2
              cy: (coords.y + crds.y) / 2
              r: 15
          .style
              fill: "url(#ligthning)"
              stroke: "#f67c34"
              "stroke-width": 3


    else
      self.r.append "circle"
      .attr
          class: "face-bg"
          cx: coords.x
          cy: coords.y
          r: 0
      .style
          fill: if runner.depth is 0 then "#d6389c" else "#fedc00"
          opacity: .2
      .transition()
      .attr "r", (70 - runner.depth * 15) + 20 - 7 * runner.depth

      self.r.append "circle"
      .attr
          class: "face"
          cx: coords.x
          cy: coords.y
          r: 0
      .style
          stroke: if runner.depth is 0 then "#d6389c" else "#fedc00"
          "stroke-width": 10
          "fill": ->
            if self.d.selectAll("#image" + runner.bound).size() is 0
              image = self.d.append "pattern"
              .attr
                  "id": "image" + runner.bound
                  x: 0
                  y: 0
                  width: 1
                  height: 1
              .append "image"
                .attr
                    x: 0
                    y: 0
                    width: 150
                    height: 150
                    "xlink:href": runner.avatar
            "url(#image" + runner.bound + ")"
      .transition()
      .attr "r", 70 - runner.depth * 15


      if runner.depth is 0
        self.r.append "circle"
        .attr
            class: "face-close"
            cx: coords.x + 90
            cy: ->
              if coords.y < 250
                coords.y + 105
              else
                coords.y - 115
            r: 0
            fill: "url(#closer)"
        .on "click", ->
            self.closePpl()

        .transition()
        .attr "r", 20

        self.r.append "circle"
        .attr
            class: "face-link"
            cx: coords.x + 45
            cy: ->
              if coords.y < 250
                coords.y + 105
              else
                coords.y - 115
            r: 20
            fill: "url(#linker)"

        .on "click", ->
            toggleSoc(coords)
        .transition()
        .attr "r", 20


        tLen = 0
        self.r.append "text"
        .text "всего заряжено " + runner.charging.length
          .attr
              class: "counter-text"
              x: ->
                tLen = this.getComputedTextLength()
                coords.x - this.getComputedTextLength()
              y: ->
                if coords.y < 250
                  coords.y + 135
                else
                  coords.y - 55 - ((70 - runner.depth * 15) + 7 - 7 * runner.depth)

        self.r.append "circle"
        .attr
            class: "text-bg-ligthning"
            cx: ->
              coords.x - tLen - 10
            cy: ->
              if coords.y < 250
                coords.y + 130
              else
                coords.y - 60 - ((70 - runner.depth * 15) + 7 - 7 * runner.depth)
            r: 10

            fill: "url(#ligthning2)"


      self.r.append "text"
      .text "@" + runner.bound
        .attr
            x: ->
              if runner.depth is 0
                coords.x + 20 - this.getComputedTextLength()
              else
                coords.x
            y: ->
              if runner.depth is 0
                if coords.y < 250
                  coords.y + 190 - ((70 - runner.depth * 15) + 7 - 7 * runner.depth)
                else
                  coords.y - 30 - ((70 - runner.depth * 15) + 7 - 7 * runner.depth)
              else
                coords.y + ((70 - runner.depth * 15) + 30 - 7 * runner.depth)
            "text-anchor": "middle"
            class: "tip-text " + if runner.depth is 0 then "main" else ""
        .style
            "font-size": if runner.depth is 0 then 30 else 24


  self.showPpl = (data) ->
    self.closePpl()
    self.r.selectAll(".shoe-bg, .shoe-img").remove()
    self.r.selectAll(".main").style "opacity", .5
    self.r.selectAll(".bg, line").style "opacity", .1


    if typeof data is "object"
      name = document.bounds[random 0, document.bounds.length - 1].bound
      coords = data
    else
      name = data.trim()
      coords = {x: 700, y: 300}

    location.hash = name

    path = collect(name)

    unless path
      location.href = "/nf"
    else
      unless typeof data is "object"
        $.get "/igData", {user: name}
        .done (res) ->
            self.data = res

      generate path, coords

  self.closePpl = ->
    location.hash = ""
    $("#wrap-home").show()
    $("#wrap-wannarun").hide()
    $("#wrap-register").hide()
    self.r.selectAll(".main, line").style "opacity", 1
    self.r.selectAll(".bg").style "opacity", .2
    self.r.selectAll(".faceline, .ligthning, .social, .counter-text, .text-bg-ligthning").remove()
    self.r.selectAll(".face")
    .transition()
    .attr "r", 0
      .remove()
    self.r.selectAll(".face-close")
    .transition()
    .attr "r", 0
      .remove()
    self.r.selectAll(".face-link")
    .transition()
    .attr "r", 0
      .remove()
    self.r.selectAll(".face-bg")
    .transition()
    .attr "r", 0
      .remove()
    self.r.selectAll(".tip-text").remove()
    self.createShoe()

  return self

app (data) ->
  b = new Bounder data.config
  d3.json "/js/data.json", (err, d) ->
    for node in d.nodes
      b.addNode node
    for link in d.links
      b.addLink link
    b.start()


  rexp = /\/user\/([a-z0-9-_])\/*/
  if rexp.test location.pathname
    history.pushState {}, "", location.pathname.replace(rexp, "/#$1")

  if location.hash is "#register"
    history.pushState {}, "", "/"
    window.Modals.instance.forceShow('modalRegister')


  $("#checkme").on "click", ->
    unless $("#namefield").val() is ""
      b.showPpl $("#namefield").val()
    return false

  $(".instagramSignin__form").on "submit", ->
    unless $("#namefield").val() is ""
      b.showPpl $("#namefield").val()
    return false

  if location.hash != '#' and location.hash != ''
    b.on "loaded", ->
      b.showPpl location.hash.substr(1)


  $("#wrap-register button, #wrap-wannarun button").on "click", (e) ->
    if b.data.placed.inspirator || b.data.placed.inspirated
      window.Modals.instance.forceShow('modalRegister')

    return false




window.onload = ->
  $(".bigBlackButton").on "click", ->
    window.Modals.instance.forceShow('modalRegister')
    _gaq.push(['_trackEvent', 'userBeh', "click", 'start', 1]);


  $("#modalRegister button.btn").on "click", ->
    _gaq.push(['_trackEvent', 'userBeh', 'click', 'register', 2]);
  app
    config:
      dim:
        w: 1200
        h: 700
