random = (min, max) ->
  Math.floor(Math.random() * (max - min + 1)) + min;

VK.init({apiId: 4483901});
userId = 0;

Bounder = ->
  self = riot.observable @

  assoc = []
  for bound, key in document.bounds
    assoc[key] = bound.bound

  socials = [
    {name: "tw"}
    {name: "fb"}
    {name: "vk"}
  ]

  document.Share =
    vk: ->
      VK.Auth.login (response) ->
        if response.session
          userId = response.session.user.id;
          VK.api 'photos.getWallUploadServer', {'user_id': userId }, (data) ->
            if (data.response)
              data = data.response
              uploadUrl = data.upload_url
              console.log self.data
              $.post '/upload',
                uploadUrl: uploadUrl,
                imagePath: "share/" + location.hash.slice(1) + (if self.data.inspirator then "-inspirator" else "-inspirated") + ".jpg",
              , (json) ->
                json = JSON.parse json
                console.log json
                if json.server
                  VK.api 'photos.saveWallPhoto', {
                    'user_id': userId,
                    'server': json.server,
                    'photo': json.photo,
                    'hash': json.hash
                  }, (data) ->
                    console.log data
                    if (data.response)
                      VK.api "wall.post", {owner_id: userId, message: "Я заряжаю друзей энергией бега! #зарядидрузей #boost #adidasrunning", attachments: data.response[0].id}, (data) ->
                        console.log "wpend"
                else
                  "json server fault"

      , 8199

    tw: ->
      this.popup "http://twitter.com/share?url=" + encodeURIComponent(location.href.replace(/#/,
        "user/")) + "&text=Я заряжаю друзей энергией бега!&hashtags=зарядидрузей,boost,adidasrunning"

    fb: ->
      this.popup "http://www.facebook.com/sharer.php?u=" + encodeURIComponent(location.href.replace(/#/, "user/"))

    popup: (url) ->
      window.open(url, '', 'toolbar=0,status=0,width=626,height=436');

  collect = (name, depth) ->
    depth = 0 unless depth?

    i = assoc.indexOf name
    if i is -1
      return false
    result = document.bounds[i]
    result.links = []
    result.depth = depth
    result.inspirators = false
    ++depth
    if depth < 3
      if document.bounds[i].charging.length is 0
        if depth is 1
          result.inspirators = []
      else
        for runner in document.bounds[i].charging
          unless runner is name
            result.links = result.links.concat collect runner, depth
    return result

  generate = (path, depth, pool) ->
    pool = [] unless pool?
    depth = 0 unless depth?
    if path.inspirators
      $(".map__connectOwn img").attr "src", path.avatar

      $.get "/api/chargedby", {name: path.bound}
      .done (inspirators) ->
          for insp in inspirators
            i = assoc.indexOf insp
            itm = document.bounds[i]
            unless i is -1
              $(".map__connected img").attr "src", itm.avatar
    else
      if depth is 0
        $(".map .map__mainPhoto img").attr "src", path.avatar
      else
        console.log path
        $(".map .map__list").append riot.render $("#map_list_item_template").html(), {image: path.avatar}
      ++depth

      for r in path.links
        if pool.indexOf(r.bound) is -1
          generate r, depth, pool

      if $(".map__list li").length > 6 then $(".map__more").show() else $(".map__more").hide()


  self.showPpl = (name) ->
    location.hash = name
    path = collect(name.trim())

    unless path
      location.href = "/nf"

    else
      $.get "/igData", {user: name}
      .done (res) ->
          self.data = res
          if res.inspirator
            $('.wrapper-index').hide();
            $('.modal-map').show();
          else
            $('.wrapper-index').hide();
            $('.modal-map-sm').show();
          generate path

          if $(".map__list li").length > 6 then $(".map__more").show() else $(".map__more").hide()


  return self


app (data) ->
  b = new Bounder


  $(".instagramSignin__submit").on "click", ->
    unless $("#namefield").val() is ""
      b.showPpl $("#namefield").val()
    return false

  $(".instagramSignin__form").on "submit", ->
    unless $("#namefield").val() is ""
      b.showPpl $("#namefield").val()
    return false


  $(".map .btn-close").on "click", ->
    $('.wrapper-index').show();
    $('.modal-map').hide();
    $('.modal-map-sm').hide();
    location.hash = ""
  rexp = /\/user\/([a-z0-9-_])\/*/
  if rexp.test location.pathname
    history.pushState {}, "", location.pathname.replace(rexp, "/#$1")

  if location.hash != '#' and location.hash != ''
    b.showPpl location.hash.substr(1)

  $('.js-showRegistration').on 'click', ->
    $('.modal-map, .modal-map-sm').hide()
    $('.modal-registration, .modal-registrationShort').hide()

    $('.modal-registrationShort').show()


window.onload = ->
  app
    config: {}

  ui = navigator.userAgent.toLowerCase()
  if ui.indexOf("android") != -1 && ui.indexOf("chrome") is -1
    $(".map__text").css
      "text-fill-color": "black"
      background: "transparent"


