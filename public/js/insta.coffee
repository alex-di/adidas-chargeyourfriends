$ document
.ready ->
    $ document
    .click (event) ->
        unless $(event.target).closest('.filter-wrap-first').length
          $(".filter-wrap-first").removeClass "active"
        unless $(event.target).closest('.filter-wrap-second').length
          $(".filter-wrap-second").removeClass "active"
    $ ".header .filter-wrap"
    .click ->
        $ @
        .addClass "active"
    app {}

Image = (image) ->
  tmpl = $("#image-template").html()
  chargeTmpl = $("#charge-template").html()
  chargeNoInspirations = $("#charge-template-noinsp").html()
  chargeInitTmpl = $("#charge-template-init").html()

  self = riot.observable @
  self.imageData = image

  self.$el = $("<div></div>", {class: "image-wrap", "data-id": self.imageData._id, "data-time": self.imageData.createdTime})
  self.render = ->
    self.$el.html riot.render tmpl, image
    iWrap = self.$el.find ".image-item"

    switch self.imageData.approved
      when 1 then iWrap.addClass "denied"
      when 2 then iWrap.addClass "approved"
      else

    charges = self.$el.find ".charge-list"
    self.imageData.charging.forEach (chargeData) ->
#      console.log chargeData
      charge = {}
      charge.chData = chargeData
      tpl = ''
      if charge.chData.type == 1
        if self.imageData.charging.length < 2
          tpl = chargeNoInspirations
        else
          tpl = chargeInitTmpl
      else
        tpl = chargeTmpl
      charge.$el = $ "<li></li>", {"data-id": charge.chData._id}
      .html riot.render (tpl), charge.chData
      unless charge.chData.sent
        charges.parent().addClass "nonsent"

      charge.$el.addClass "sent" if charge.chData.sent

      charge.$el.on "click", "li", ->
        $(this).find "textarea"
        .show()
        $(this).one "mouseleave", "textarea", ->
          $(this).hide()

      charge.$el.on "click", ->
        $(this).find "textarea"
        .show()
        .focus()
        $(this).one "mouseleave", "textarea", ->
          $(this).hide()

      charge.$el.on "focus", "textarea", ->
        $(this).select()
      charge.$el.on "click", "textarea", ->
        $(this).select()

      charge.$el.on "click", "a.sent-indicator", ->
        for ch in self.imageData.charging
          if ch._id is charge.chData._id
            ch.sent = true if ch._id is charge.chData._id
            ch.sentDate = Date.now()

        self.trigger "change"
        return false

      charges.append charge.$el

  self.one "load", ->
    self.render()

    self.$el.on "click", ".approve-controls .approve", ->
      self.imageData.approved = 2
      self.trigger "change"

    self.$el.on "click", ".approve-controls .deny", ->
      self.imageData.approved = 1
      self.trigger "change"

  self.on "change", ->
    self.render()


  self.trigger "load"
  return self

app (data) ->
  root = $(".content")

  timer = null

  data.on "newImage", (image) ->
    console.log "app: new image"
    item = new Image image

    item.on "change", ->

      data.trigger "changedImage", item.imageData

    root.append item.$el

    clearTimeout timer if timer?
    timer = setTimeout ->
      sortIt()
    , 5000

  data.on "newPage", (cb) ->
    root.html("")
    cb()


  sortIt = ->
    items = root.children ".image-wrap"
    items.sort (a,b) ->
      an = a.getAttribute('data-time')
      bn = b.getAttribute('data-time')

      if (an > bn)
        return 1
      if(an < bn)
        return -1
      return 0
    items.detach().appendTo(root)

app (data) ->
  socket = io "/"
  $("#page-selector").val(parseInt(location.hash.slice(1)) || 0)
  $("#page-selector").on "change", ->
    page = $(this).val()
    location.hash = page

    data.trigger "newPage", ->
      socket.emit "getPage", page


  socket.on "connect", () ->
    socket.emit "getPage", parseInt(location.hash.slice(1)) || 0

  socket.on "newImage", (media) ->

#    console.log "socket: new image", media
    data.trigger "newImage", media

  data.on "changedImage", (image) ->
#    console.log "app: changed image", image
    socket.emit "changedImage", image