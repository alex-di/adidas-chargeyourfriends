!function ($) {
    
    $(function () {

        'use strict';

        $(document).ready(function(){
            var isMacLike = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i) ? true : false;
            if (isMacLike === true) {
                $('html').addClass('mac');
            }

            Modernizr.load({
                test: Modernizr.input.placeholder,
                nope: "js/fallbacks/jquery.placeholder.js"
            });

            var testEl = document.createElement( "x-test" ),
                supportsWebkitBackgroundClipText = typeof testEl.style.webkitBackgroundClip !== "undefined" && ( testEl.style.webkitBackgroundClip = "text", testEl.style.webkitBackgroundClip === "text" );

            if (supportsWebkitBackgroundClipText !== true) {
                $('html').addClass('no-backgroundClipText');
            }

            $('.js-showProduct').on('click', function(){
                $('.wrapper-index').hide();
                $('.modal-product').show();
            });

            $('.js-closeProduct').on('click', function(){
                $('.modal-product').hide();
                $('.wrapper-index').show();
            });

            $('.js-showRegistration').on('click', function(){
                $('.modal-map').hide();
                $('.modal-registrationShort').show();
            });

            $('.js-openSelect').on('click', function(){
                $(this).toggleClass('opened');
            });

            $('.js-showMoreConnections').on('click', function(){
                $('.js-mapList').addClass('nolimit');
                $(this).parent().hide();
            });

            $('.js-selectList').on('click', 'li', function(e){
                e.stopPropagation();

                var $parent = $(this).parent().parent().parent(),
                    $value = $($parent).find('.customSelect__value'),
                    value = $(this).data('value'),
                    valueText = $(this).text(),
                    input = $($parent).data('forinput');

                $(this).parent().find('.selected').removeClass('selected');
                $(this).addClass('selected');

                $('#' + input).val(value);
                $($value).html(valueText);

                $($parent).removeClass('opened');

            });

            $('.js-validateForm').on('submit', function(e){
                var $validate = $(this).find('.js-validate'),
                    isError = false;

                $(this).find('.invalid').removeClass('invalid');
                $(this).find('.js-invalidMessage').html('');
                
                $($validate).each(function(){

                    var value = $.trim($(this).val()),
                        $parent = $(this).parent(),
                        data = $(this).data(),
                        isRequired = (data.required == true || $(this).attr('required') == true) ? true : false,
                        isValid = false,
                        minLength = (data.minlength || $(this).attr('minlength')),
                        maxLength = (data.maxlength || $(this).attr('maxlength')),
                        defaultValue = $(this).attr('placeholder') || '';
                    
                    if (!$($parent).hasClass('hidden')) {

                        if (isRequired) {
                            if (value == '' || value == defaultValue) {
                                $($parent).addClass('invalid')
                                          .find('.js-invalidMessage')
                                          .html('Поле обязательно для заполнения');
                                isError = true;
                            }
                        }

                        if (typeof minLength !== 'undefined') {
                            isValid = (value.length < minLength) ? true : false;

                            if (isValid === false) {
                                $($parent).addClass('invalid')
                                          .find('.js-invalidMessage')
                                          .html('Значение поля слишком короткое');

                                isError = true;
                            }   
                        }

                        if (typeof maxLength !== 'undefined') {
                            isValid = (value.length < maxLength) ? true : false;

                            if (isValid === false) {
                                $($parent).addClass('invalid')
                                          .find('.js-invalidMessage')
                                          .html('Значение поля слишком длинное');
                                isError = true;
                            }      
                        }

                        if (typeof data.validator !== 'undefined' && !(value == '' || value == defaultValue)) {
                            switch(data.validator) {
                                case 'number':
                                    var regex = /\d+/;
                                    if (!regex.test(value)) {
                                        $($parent).addClass('invalid');
                                        isError = true;
                                    }
                                break;
                                case 'name':
                                    var regex = /^[A-Za-zА-Яа-я ]+$/;
                                    if (!regex.test(value)) {
                                        $($parent);
                                        isError = true;
                                        $($parent).addClass('invalid')
                                                  .find('.js-invalidMessage')
                                                  .html('В поле ФИО могут быть только буквы и пробел');
                                    }
                                break;
                                case 'email':
                                    var lastAtPos = value.lastIndexOf('@'),
                                        lastDotPos = value.lastIndexOf('.'),
                                        isValid = (lastAtPos < lastDotPos && lastAtPos > 0 && value.indexOf('@@') == -1 && lastDotPos > 2 && (value.length - lastDotPos) > 2) ? true : false;

                                    if (isValid === false) {
                                        $($parent).addClass('invalid')
                                                  .find('.js-invalidMessage')
                                                  .html('Неверный формат email');

                                        isError = true;
                                    }
                                break;
                            }
                        }
                    }
                });

                if (isError) {
                    e.preventDefault();
                    return false;
                }
            });
        
            
        });
    });

}(window.jQuery);   