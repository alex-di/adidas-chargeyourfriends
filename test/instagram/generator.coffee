mongoose = require "mongoose"
mongoose.connect "mongodb://localhost:27017/charge"
should = require "should"

suite "Generator", ->
  generator = require "../../modules/instagram/instagram-bound-generator"
  test "Generator should have method `generate`", ->
    generator.should.have.method "generate"
    generator.generate.should.be.type "function"

