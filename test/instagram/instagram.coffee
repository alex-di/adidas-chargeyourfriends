request = require "superagent"
should = require "should"

mongoose = require "mongoose"
mongoose.connect "mongodb://localhost:27017/chargeTest"

suite "Instagram", ->
  before ->
    mongoose.connection.collections['media'].drop (err) ->
      throw err if err
      console.log('collection dropped');


  this.timeout 10000
  riot = require "../../riotjs"
  app = require "../../riot"
  .app
  app require "../../modules/server/server"
  app require "../../modules/instagram/instagram-parser"

  test "Checking tags", (done) ->
    app.on "ready", (data) ->
      data.ig.on "tagChecked", (items) ->
        items.should.exists
        done()

      data.ig.checkTag "energyfriendtest"


    app
      riot: riot
      env:
        PORT: 10041