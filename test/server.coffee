
request = require "superagent"
should = require "should"

riot = require "../riotjs"
app = require "../riot"
.app
suite "Server", ->
  app require "../modules/server/server"
  test "Adding routes", (done) ->
    app.on "ready", (data) ->
      console.log data
      data.server.should.have.property "addRoute"
      data.server.addRoute "get", '/test', (req, res) ->
        res.send 200, "Success test"
      request.get "localhost:10400/test"
      .end (res) ->
          res.text.should.eql "Success test"
          done()


    app
      riot: riot
      env:
        PORT: 10400