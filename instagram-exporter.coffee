mongoose = require "mongoose"
path = require "path"
fs = require "fs"
config = require path.join path.dirname(require.main.filename), "./config"
excel = require "excel-export"
Media = require "./modules/instagram/models/media"
moment = require "moment"
mongoose.connect config.db

conf = {cols: [], rows: []}
conf.cols = [
  {type: "string", "caption": "Ник инспиратора", width: 16}
  {type: "string", "caption": "Дата когда запарсили фото", width: 14}
  {type: "string", "caption": "Ссылка на фото", width: 30}
  {type: "string", caption: "Ник отмечаемого (инспиратора или заряженного)", width: 16}
  {type: "string", "caption": "Дата, когда отметили", width: 14}
  {type: "string", caption: "Причина, если отмечаемого не отметили"}
]

Media.find({}).sort({createdTime: -1}).exec (err, result) ->
  for item in result
    first = true

    for charge in item.charging
      sentDate = if charge.sent then moment(charge.sentDate).format("YYYY-MM-DD HH:mm") else "not sent"
      sentDate = item.createdTime unless sentDate?
      user = if first then item.user.username else ''
      cDate = if first then moment(item.createdTime).format("YYYY-MM-DD HH:mm") else ''
      link = if first then item.link else ""

      media = [user, cDate, link, charge.user, sentDate, "" ]
      conf.rows.push media
      first = false
    conf.rows.push ['', '', '', '', '', '']

  result = excel.execute conf
  fs.writeFile "public/chargefriends-inspirations-export-" + moment().format('YYYY-MM-DD') + ".xlsx", result, "binary", (err) ->
    console.log err if err
    console.log "Export ended!"
    process.kill()
#process.send "200"